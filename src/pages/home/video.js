import React from 'react';
import {Media} from 'react-bootstrap'

const Video = () => {
    return(
        <Media>
            <iframe 
                width="560" 
                height="315" 
                src="https://www.youtube.com/embed/N7pSzg8ygFM" 
                frameborder="0" 
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                allowfullscreen />
        </Media>
    );
}

export default Video;