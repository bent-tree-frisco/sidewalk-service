import React from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import Video from './video';
import Copy from './copy';
import '../page.css';

const Home = () => {
    return (
        <Container className="baseline" fluid="md">
            <Row>
                <Col className="col-padded"><Video /></Col>
                <Col className="col-padded"><Copy /></Col>
            </Row>

        </Container>
    )
}

export default Home;