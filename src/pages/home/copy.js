import React from 'react';

const Copy = () => {
    return (
        <div>
            <p>2020 presents a unique situation for all of us. In an effort to be safe and reduce the spread of COVID-19, this is the first time in our nation’s history we can’t “go to church” on Easter Sunday.</p>

            <p>But, what if we didn’t need a building to “go to church”? What if, we maintained Social Distancing guidelines and went to church “together” in our front yards this Easter? We’re calling this a Sidewalk Service.</p>

            <p>The vision is for people to "go to church" with their neighbors while maintaining Social Distancing Guidelines. We'll grab our lawn chairs, an internet connected device, head to our front yards, and "go to church" with our neighbors this Easter for a brief 20-minute service.</p>

            <a className="btn btn-link" href="https://www.facebook.com/events/1726702364149812/">RSVP and invite your neighbors</a>
            <a className="btn btn-link" href="https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/social-distancing.html">View the CDC's Social Distancing guidelines</a>
        </div>
    );
}

export default Copy;