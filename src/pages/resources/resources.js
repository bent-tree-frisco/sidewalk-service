import React from 'react';
import {Row, Col, Container, Media, Accordion, Card, Button} from 'react-bootstrap';
import '../page.css';
import SidewalkChalk from './sidewalk-service-guide.jpg';
import RaveCard from './rave-card.jpg';

const Resources = () => {
    return(
        <Container>
            <Row>
                <Col align="center">
                    <h2>Resources</h2>
                    <p>The Sidewalk Service is about being with your neighbors in. Here are a couple of ways to creatively invite them.</p>
                </Col>
            </Row>
            <Row>
            
            </Row>
            <Row>
                <Col>
                    <p>Print up this invite card, cut them, personalize them, and then invite your neighbors to join you in their front yard.</p>
                </Col>
                <Col>
                    <Accordion>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                <h5>Invite Card</h5>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Media>
                                    <img
                                        className="responseImg"
                                        src={RaveCard} 
                                        alt="Sidewalk Service Easter Sunday Rave Card" 
                                        />
                                    </Media>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>

                    </Accordion>
                </Col>
            </Row>
            <Row>
                <Col>
                    <p>Engage your neighbors with sidewalk chalk. Follow the guide below for a creative way to engage your neighbors as they go for walks by your house.</p>
                </Col>
                <Col>
                    <Accordion>
                        <Card>
                            <Accordion.Toggle as={Card.Header} variant="link" eventKey="0">
                                <h5>Sidewalk Chalk Guide</h5>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Media>
                                    <img
                                        className="responseImg"
                                        src={SidewalkChalk} 
                                        alt="Sidewalk Service Easter Sunday Sidewalk Chalk Instructions" 
                                        />
                                    </Media>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>

                    </Accordion>
                </Col>
            </Row>
            
        </Container>
    );
}

export default Resources;