import React from 'react';
import Copy from './copy';
import {Container, Row, Col} from 'react-bootstrap';
import SocialLinks from '../../components/social/social-links';
import '../page.css';

const About = () => {
    return(
        <Container className="baseline" fluid="md">
            <Row>
                <Copy />
            </Row>
        </Container>
    );
}

export default About;