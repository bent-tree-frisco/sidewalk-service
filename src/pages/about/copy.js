import React from 'react';
import { Col } from 'react-bootstrap';

const AboutCopy = () => {
    return(
        <div>
            <Col align="center">
                <h2>The Easter Sunday Sidewalk Service is an initiative of Bent Tree Frisco</h2>
            </Col>
            <Col>
                <h3>Our Heart</h3>
                <p>No matter your past, background, or where you are on your journey, you are welcome at Bent Tree Frisco. We are a relationally driven, biblically based church that loves Jesus. We’re passionate to serve and meet the needs of our city by making a tangible difference here.</p>

                <h3>Our Story</h3>
                <p><b>We do not simply want to play church.</b> We want to be a church that makes real and tangible differences in her city by serving those in need; especially the marginalized. We are a church FOR the city of Frisco.</p>

                <p><b>We are relationally driven.</b> Creating safe environments where people are known and loved. Where people can be real about the ups and downs of life. Supporting and encouraging each other with truth, love, and grace.</p>

                <p><b>We are biblically based.</b> Whether it’s Family Ministry, Adult Studies, or Sunday services, we will always preach from scripture. We believe the entire bible is inspired by God and useful to teach us what is true. It corrects us when we are wrong and teaches us to do what is right because God uses it to prepare and equip us to grow spiritually and serve our community.</p>

                <p><b>We love Jesus.</b> Jesus flipped the economy of salvation upside down. He went out of his way to serve the outcasts of society and was critical of hypocritical religion. He sustains us, comforts us, and guides us in every aspect of life.</p>

                Learn more about Bent Tree Frisco by visiting our <a href="http://benttree.org/frisco">website</a> or <a href="http://facebook.com/benttreefrisco">Facebook page</a>! 
                
            
            </Col>

        </div>
    )
}

export default AboutCopy;