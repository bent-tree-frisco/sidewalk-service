import React from 'react';
import {Media} from 'react-bootstrap'

const Video = () => {
    return(
        <React.Fragment>
            <div id="fb-root"></div>

            <div className="fb-video facebook-live" data-href="https://www.facebook.com/benttreefrisco/videos/2439656572802352/" 
                data-width="auto"
                data-show-text="false"
                data-allowfullscreen="true">
                    
            </div>
        </React.Fragment>
    );
}

export default Video;