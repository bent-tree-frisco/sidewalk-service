import React from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import Video from './video';
import Copy from './copy';
import '../page.css';

const EasterHome = () => {
    return (
        <Container className="baseline" fluid="md">
            <Row>
                <Col className="col-padded" align="center"><Video /></Col>   
                
            </Row>
                
            <Row>
            <Col className="col-padded" align="center"><Copy /></Col>             
            </Row>

        </Container>
    )
}

export default EasterHome;