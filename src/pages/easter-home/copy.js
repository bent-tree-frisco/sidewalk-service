import React from 'react';
import '../page.css';

const Copy = () => {
    return (
        <div>
            <p>This Easter is different than any we’ve ever experienced, so we’re celebrating differently than we ever have and hope you enjoy our sidewalk service.</p>

            <p>Please share your photos using #sidewalkservice</p>

            <p>If you’d like to learn more about the study Michael referenced, you can click the link below.</p>

            <a className="btn btn-link" href="https://login.benttree.org/connect/eventdetail/312804">Learn More</a>

            <p>If you’d like more information about Bent Tree Frisco please click the about button at the top of the page.</p>
        </div>
    );
}

export default Copy;