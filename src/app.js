import React from 'react';
import Nav from './components/nav';
import Home from './pages/home/home';
import About from './pages/about/about';
import Resources from './pages/resources/resources';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import Footer from './components/footer/footer';
import EventBanner from './components/event-banner/event-banner';
import EasterHome from './pages/easter-home/home';


const app = () => {
    return (
        <React.Fragment>
        <Router>
            <Nav />
            <EventBanner />
            <Switch>                    
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/resources">
                    <Resources />
                </Route>
                <Route path="/">
                    <EasterHome />
                </Route>                
            </Switch>
        </Router>
        <Footer />
        </React.Fragment>
    )
}

export default app;