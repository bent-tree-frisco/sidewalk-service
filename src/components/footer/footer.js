import React from 'react';
import SocialLinks from '../social/social-links';
import './footer.css';
import { Container } from 'react-bootstrap';

const Footer = () => {
    return(
        <Container fluid="md">
            <footer className="pt-4 my-md-5 pt-md-5">
                <SocialLinks />            
            </footer>
        </Container>
    );
}

export default Footer;