import React, { useEffect, useState } from 'react';
import './countdown.css';

const calculateTimeLeft = () => {
    const difference = +new Date("2020-04-12T10:00:00-05:00") - +Date.now();
    let timeLeft = {};

    if(difference > 0) {
        timeLeft = {
            days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            hours: Math.floor((difference / (1000 * 60 * 60)) %24),
            minutes: Math.floor((difference / 1000 / 60) %60),
            seconds: Math.floor((difference / 1000) % 60)
        }
    }

    return timeLeft;
}

const Countdown = () => {

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);
    });


    const timerComponents = [];

    Object.keys(timeLeft).forEach(interval => {
        if(!timeLeft[interval]) {
            return;
        }

        timerComponents.push(
            <li key={interval} className="countdown--timeLeft-container">
                <span className="countdown--timeLeft-value">{timeLeft[interval]}</span>
                <span className="countdown--timeLeft-label">{interval}</span>
            </li>
        )
    })

    return(
        <ul className="countdown">
            {timerComponents.length ? timerComponents 
                : <span className="shout-it-loud">
                    He is Risen!
                </span>}
        </ul>
    );
}

export default Countdown;