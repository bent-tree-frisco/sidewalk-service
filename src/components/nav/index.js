import React from 'react';
import Logo from './logo/logo';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import './navbar.css';
import Countdown from './countdown/countdown';

const nav = () => {
    return (
        <Navbar className="border-bottom" expand="lg" justify-content-between="true">
            <Logo />
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav>
                    <Nav.Item><Link className="nav-link" to="/">Home</Link></Nav.Item>
                    <Nav.Item><Link className="nav-link" to="/about">About</Link></Nav.Item>
                    <Nav.Item><Link className="nav-link" to="/resources">Resources</Link></Nav.Item>
                </Nav>
                
            </Navbar.Collapse>
            <Navbar.Collapse className="justify-content-end"><Countdown /></Navbar.Collapse>
        </Navbar>
    )
}

export default nav;