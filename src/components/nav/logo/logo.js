import React from 'react';
import {Navbar} from 'react-bootstrap';
import BentTreeFriscoLogo from './bent-tree-logo';

const logo = () => {

    return (
        <Navbar.Brand href="http://benttree.org/frisco">
            <BentTreeFriscoLogo />
        </Navbar.Brand>
    )
}

export default logo;