import React from 'react';
import {Row, Col} from 'react-bootstrap';
import './social.css';

const SocialLinks = () => {
    return(
        <Row>
            <Col></Col>
            <Col align="end">
                <a className="btn btn-link social-links" href="https://www.facebook.com/BentTreeFrisco"><i className="fab fa-facebook fa-w-16 fa-3x"></i></a>
                <a className="btn btn-link social-links" href="https://twitter.com/btfrisco"><i className="fab fa-twitter fa-w-16 fa-3x"></i></a>
            </Col>
        </Row>
        
    );
}
//<a className="btn btn-link social-links" href="https://benttree.org/frisco"><i class="fas fa-share fa-w-16 fa-3x"></i></a>
export default SocialLinks;