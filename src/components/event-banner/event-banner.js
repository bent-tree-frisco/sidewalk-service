import React from 'react';
import { Jumbotron, Media, Container } from 'react-bootstrap';
import './event-banner.css';
import Banner from './FB-event-banner.png';

const EventBanner = () => {
    return (
        <Container className="justify-content-center">
            <Jumbotron>
                <Media>
                    <img
                        className="responseImg"
                        src={Banner} 
                        alt="Sidewalk Service Easter Sunday" />
                </Media>
            </Jumbotron>
        </Container>
    );
}

export default EventBanner;